package in.lnt.parser;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonParser {
	private JsonParser(){
		
	}
	static ObjectMapper mapper = new ObjectMapper();

	

	public static Map<String, JSONObject> parseJSON(InputStream inputStreamJSON)
			throws IOException {
		Map<String, org.json.JSONObject> mapcolumnList = new HashMap<>();

		Map map = mapper.readValue(inputStreamJSON, Map.class);
		
		ArrayList arrList = (ArrayList) map.get("columns");
		for (int i = 0; i < arrList.size(); i++) {
			LinkedHashMap linkedHashMap = (LinkedHashMap) arrList.get(i);
			gl(linkedHashMap.keySet(), linkedHashMap, mapcolumnList);
		} 
		return mapcolumnList;
	}

	static void gl(Set set, LinkedHashMap linkedHashMap, Map<String, JSONObject> mapcolumnList) {
		org.json.JSONObject obj = new org.json.JSONObject();
		
		for (Object str : set) {
			if (linkedHashMap.get(str) instanceof String) {
				obj.append((String) str, (String) linkedHashMap.get(str));
			} else if (linkedHashMap.get(str) instanceof Object) {
				obj.append((String) str, linkedHashMap.get(str));
			}
		}
		if (obj.get("code") instanceof org.json.JSONArray) {
			org.json.JSONArray jarray = (JSONArray) obj.get("code");
			mapcolumnList.put((String) jarray.get(0), obj);
		}
	}

}