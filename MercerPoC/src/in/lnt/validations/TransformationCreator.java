package in.lnt.validations;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import in.lnt.enums.TransformationTypes;

public class TransformationCreator {
	private static final Logger logger = LoggerFactory.getLogger(TransformationCreator.class);
	static String[] dateFormatList = new String[] { "MM/dd/yyyy", "dd/MM/yyyy", "yyyy-dd-MM", "yyyy-MM-dd" };

	private TransformationCreator() {
		
	}
	
	public static void transformationsCreator(String key, String tranformationType, Map<String, String> data1) {
		if (EnumUtils.isValidEnum(TransformationTypes.class, tranformationType)) {

			HashMap<String, String> data = (HashMap<String, String>)data1;
			switch (TransformationTypes.valueOf(tranformationType.toUpperCase())) {
			case EXTRACTYEAR:
				dateTransformation(data, key, "yyyy");
				break;
			case EXTRACTDAY_NUMERIC:
				dateTransformation(data, key, "DD");
				break;
			case EXTRACTDAY_TEXT:
				dateTransformation(data, key, "EEEE");
				break;
			case EXTRACTMONTH:
				dateTransformation(data, key, "MM");
				break;
			}
		}
	}

	static void dateTransformation(HashMap<String, String> data, String key, String dateFormat) {
		try {
			Date date = DateUtils.parseDateStrictly(data.get(key), dateFormatList);
			data.put(key, String.valueOf(new SimpleDateFormat(dateFormat, Locale.ENGLISH).format(date)));
		} catch (Exception e) {
			logger.info("dataTransformtion {}",e.getMessage());

		}
	}
}
