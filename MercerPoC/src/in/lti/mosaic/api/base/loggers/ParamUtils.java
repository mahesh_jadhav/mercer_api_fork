package in.lti.mosaic.api.base.loggers;

public class ParamUtils {
	private ParamUtils() {
		throw new IllegalStateException("ParamUtils class");
	}
	public static String getString(Object... values) {
		if (values == null)
			return "";

		StringBuilder rec = new StringBuilder("");
		for (Object value : values) {
			if (value != null)
				rec.append(value.toString()).append(" ");
		}	
		
		return rec.toString();
	}

}
