package com.lti.mosaic.function.def;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.lti.mosaic.derby.DerbyConnection;
import com.lti.mosaic.derby.MercerDBCP;
import com.lti.mosaic.parser.enums.FunctionEnum;
import com.lti.mosaic.parser.utils.DataUtil;
import com.lti.mosaic.parser.utils.ExpressionBuilderConstants;

public class InversionAndRangeCheckFunctions {
	
	private static final String SALARY_CALCULATED_FOR_ROW = "Salary calculated for row : {} :: {}";

	private static final String CONNECTION_TO_MERCER_DB_IS_OPENED = "Connection to MercerDB is opened...";

	private static final Logger logger = LoggerFactory.getLogger(InversionAndRangeCheckFunctions.class);

	private List<String[]> csvData = new ArrayList<>();
	private Map<String, Object> otherDataMap = new HashMap<>();

	private static final String GROUP_BY_COL = "GROUP_BY_COL";
	private static final String SELECT_COL = "SELECT_COL";
	public static final String FALSE = "false";
	public static final String TRUE = "true";
	private static final String MAXIQ_DELIMATOR = "";
	private static final String NO_RESULT = "No Result!";

	
	public InversionAndRangeCheckFunctions(List<String[]> data, Map<String, Object> otherData) {
		this.csvData = data;
		this.otherDataMap = otherData;
	}

	@SuppressWarnings("unchecked")
	public Value executeInversionAndRangeCheckFunctions(FunctionEnum functionName,List<Value> args) {		
		JSONArray lookupMap = null;
		Double lookupValue = null;
		boolean isLookUpValueBool = false;
		JSONObject dataMap = null;
		JSONArray result = null;
		Set<Object> dataKeys = null;

		switch (functionName) {
		case AGGREGATERANGECHECK:
			result = new JSONArray();
			logger.info("InversionAndRangeCheckFunctions >> executeInversionAndRangeCheckFunctions >> args : {}" , args);
			try {

				dataMap = new JSONObject(args.get(0).toString());
				// Getting keys From dataArray
				dataKeys = dataMap.keySet();
				
				try {
					lookupValue = Double.valueOf(args.get(args.size() - 2).toString());
					isLookUpValueBool = true;
				} catch (Exception e) {
					lookupMap = new JSONArray(args.get(args.size() - 2).toString());
				}
				
				if (dataMap.length() >= 1) {

					List<String> lookUpHeader = new ArrayList<>();
					List<String> lookUpKeys = new ArrayList<>();
					
					setLookUpHeaderAndKeys(lookupMap, lookUpHeader, lookUpKeys);

					Iterator dataKeyIterator = dataKeys.iterator();
					while (dataKeyIterator.hasNext()) {
						performComparisonForAggregateRangeCheck(args, lookupMap, lookupValue,
								isLookUpValueBool, dataMap, result, lookUpKeys, dataKeyIterator);
					}
				}
			} catch (Exception e) {
				logger.error("InversionAndRangeCheckFunctions >> executeInversionAndRangeCheckFunctions >> Exception occured : AGGREGATERANGECHECK : {}" , e.getMessage());
				logger.info("{}","Exiting from  : >> InversionAndRangeCheckFunctions >> executeInversionAndRangeCheckFunctions >> AGGREGATERANGECHECK");
			}
			return  new Value(result);

		case INVERSIONCHECK:
			if (checkValidInputForInversion(args)) {
				
				JSONArray inversionResult = new JSONArray();
				String[] removeKeys = { GROUP_BY_COL, SELECT_COL, "COUNT" };
				
				dataMap = getInputInputForInversion(args);
				
				List<Integer> keysInteger = new ArrayList<>();
				List<String> keysString = new ArrayList<>();
				String groupByCol = "";

				groupByCol = getAndSortKeysForInversion(dataMap, keysInteger, keysString);

				for (int i = 0; i < keysInteger.size(); i++) {

					JSONObject dataObjectFirst = (JSONObject) dataMap
							.get(groupByCol + MAXIQ_DELIMATOR + keysInteger.get(i));

					iterateNextObjectsForComaparisonForInversion(dataMap, inversionResult, removeKeys, keysInteger,
							groupByCol, i, dataObjectFirst);
				}
				return new Value(inversionResult);
			}
			return null;
		default:
			break;
		}
		return new Value("");
	}


	/**
	 * @param args
	 * @return
	 */
	private boolean checkValidInputForInversion(List<Value> args) {
		return null != args && null != args.get(0) && !args.get(0).toString().equals(NO_RESULT);
	}

	/**
	 * @param dataMap
	 * @param keysInteger
	 * @param keysString
	 * @return
	 * @throws NumberFormatException
	 */
	private String getAndSortKeysForInversion(JSONObject dataMap, List<Integer> keysInteger, List<String> keysString) {
		String groupByCol;
		for (Object object : dataMap.keySet()) {
			String[] keySplit = String.valueOf(object).split(MAXIQ_DELIMATOR);
			if (NumberUtils.isNumber(keySplit[keySplit.length - 1]))
				keysInteger.add(Integer.valueOf(keySplit[keySplit.length - 1].trim()));
			else
				keysString.add(keySplit[keySplit.length - 1]);
		}
		
		groupByCol = String.valueOf(dataMap.keys().next()).split(MAXIQ_DELIMATOR)[0];
		
		if (!keysInteger.isEmpty())
			Collections.sort(keysInteger);
		else
			Collections.sort(keysString);
		
		return groupByCol;
	}

	/**
	 * @param args
	 * @param dataMap
	 * @return
	 * @throws JSONException
	 */
	private JSONObject getInputInputForInversion(List<Value> args) {
		try {
			return new JSONObject(args.get(0).toString());
		}catch(JSONException e) {
			logger.error("{}","Mandatory columns not found.");
			throw e;
		}
	}

	/**
	 * @param dataMap
	 * @param inversionResult
	 * @param removeKeys
	 * @param keysInteger
	 * @param groupByCol
	 * @param i
	 * @param dataObjectFirst
	 * @throws JSONException
	 * @throws NumberFormatException
	 */
	private void iterateNextObjectsForComaparisonForInversion(JSONObject dataMap, JSONArray inversionResult,
			String[] removeKeys, List<Integer> keysInteger, String groupByCol, int i, JSONObject dataObjectFirst) {
		if (i + 1 < keysInteger.size()) {

			JSONObject dataObjectSecond = (JSONObject) dataMap
					.get(groupByCol + MAXIQ_DELIMATOR + keysInteger.get(i + 1));

			//By Default > Used
			if (Double.parseDouble(dataObjectFirst.get(keysInteger.get(i) + "").toString()) > Double
					.parseDouble(dataObjectSecond.get(keysInteger.get(i + 1) + "").toString())) {
				inversionResult
						.put(buildInversionObject(dataObjectFirst, removeKeys, keysInteger, i, true));
			} else {
				inversionResult
						.put(buildInversionObject(dataObjectFirst, removeKeys, keysInteger, i, false));
			}

		} else {
			inversionResult.put(buildInversionObject(dataObjectFirst, removeKeys, keysInteger, i, true));
		}
	}

	/**
	 * @param args
	 * @param lookupMap
	 * @param lookupValue
	 * @param isLookUpValue
	 * @param isLookUpValueBool
	 * @param dataMap
	 * @param result
	 * @param lookUpKeys
	 * @param dataKeyIterator
	 * @throws JSONException
	 * @throws NumberFormatException
	 */
	private void performComparisonForAggregateRangeCheck(List<Value> args, JSONArray lookupMap, Double lookupValue,
			boolean isLookUpValueBool, JSONObject dataMap, JSONArray result, List<String> lookUpKeys, Iterator dataKeyIterator) {
		String key = (String) dataKeyIterator.next();
		JSONObject dataObject = (JSONObject) dataMap.get(key);
		String objectSearchKey = (String) dataObject.get((String) dataObject.get(GROUP_BY_COL));
		
		String isLookupValuePresent = (null != lookupValue) ? TRUE : FALSE;

		if (null != objectSearchKey && (isLookUpValueBool || lookUpKeys.contains(objectSearchKey))) {
			
			String operator = args.get(args.size() - 1).asString();
			double lhs = Double.parseDouble(dataObject.get(objectSearchKey).toString());
			
			switch (isLookupValuePresent) {
			case TRUE:
				double rhs = lookupValue ;
				
				logger.info("Data : key - {} ,  value - {}  , LookUp : key  Value -  {} / Operator : {} ",
						objectSearchKey, dataObject.get(objectSearchKey), lookupValue,
						args.get(args.size() - 1));
				
				if (AggregateFunctions.performComparisonOperation(operator, lhs, rhs)) {
					logger.info("Data : key - {} ,  value - {}  , LookUp : key  Value -  {} / Operator : {} ",
							objectSearchKey, dataObject.get(objectSearchKey), lookupValue, args.get(args.size() - 1));

					putResultIntoResultObject(result, dataObject, objectSearchKey);
					break;
				}
				break;
			case FALSE:
				for (int i = 1; i < lookupMap.length(); i++) {
					if(iterateLookUpMap(args, lookupMap, result, dataObject, objectSearchKey, operator, lhs, i)) {
						break;
					}
				}
				break;
			default: 
				break;
			}
		}
	}

	/**
	 * @param args
	 * @param lookupMap
	 * @param result
	 * @param dataObject
	 * @param objectSearchKey
	 * @param operator
	 * @param lhs
	 * @param i
	 * @return 
	 * @throws JSONException
	 * @throws NumberFormatException
	 */
	private boolean iterateLookUpMap(List<Value> args, JSONArray lookupMap, JSONArray result, JSONObject dataObject,
			String objectSearchKey, String operator, double lhs, int i) {
		double rhs;
		JSONArray lookUpObject = null;

		if (null != lookupMap.get(i)) {
			lookUpObject = (JSONArray) lookupMap.get(i);
		} else
			return false;

		if (checkValidityOfKeyAndValue(dataObject, objectSearchKey, lookUpObject)) {

			rhs = Double.parseDouble(lookUpObject.get(1).toString());

			if (AggregateFunctions.performComparisonOperation(operator, lhs, rhs)) {
				logger.info(
						"Data : key - {} ,  value - {}  , LookUp : key {} , Value -  {} / Operator : {} ",
						objectSearchKey, dataObject.get(objectSearchKey), (JSONArray) lookupMap.get(i),
						lookUpObject.get(1), args.get(args.size() - 1));

				putResultIntoResultObject(result, dataObject, objectSearchKey);
				return true;
			}
		}
		return false;
	}

	/**
	 * @param dataObject
	 * @param objectSearchKey
	 * @param lookUpObject
	 * @return
	 * @throws JSONException
	 */
	private boolean checkValidityOfKeyAndValue(JSONObject dataObject, String objectSearchKey, JSONArray lookUpObject) {
		return (null != lookUpObject.get(0)
				&& lookUpObject.get(0).toString().trim().equals(objectSearchKey.trim()))
				&& (null != lookUpObject.get(1) && null != dataObject.get(objectSearchKey).toString());
	}

	/**
	 * @param result
	 * @param dataObject
	 * @param objectSearchKey
	 */
	private void putResultIntoResultObject(JSONArray result, JSONObject dataObject, String objectSearchKey) {
		String[] removeKeys = { GROUP_BY_COL, SELECT_COL, objectSearchKey };
		for (String removeKey : removeKeys) {
			dataObject.remove(removeKey);
		}
		result.put(dataObject);
	}

	/**
	 * @param lookupMap
	 * @param lookUpHeader
	 * @param lookUpKeys
	 * @throws JSONException
	 */
	private void setLookUpHeaderAndKeys(JSONArray lookupMap, List<String> lookUpHeader, List<String> lookUpKeys) {
		if (null != lookupMap) {
			// get common key
			if (lookupMap.length() >= 1) {
				lookUpHeader.add(String.valueOf(((JSONArray) lookupMap.get(0)).get(0)));
			}
			// Get all LookUp Keys
			for (int i = 1; i < lookupMap.length(); i++) {
				lookUpKeys.add(String.valueOf(((JSONArray) lookupMap.get(i)).get(0)));
			}
		}
	}
	
	JSONObject buildInversionObject(JSONObject dataObjectFirst, String[] removeKeys, List<Integer> keysInteger, int i, boolean putNextObject){
		for (String removeKey : removeKeys) {
			dataObjectFirst.remove(removeKey);
		}
		dataObjectFirst.remove(keysInteger.get(i) +"");
		if(putNextObject && (i + 1 < keysInteger.size()))
			dataObjectFirst.put("LINKED", keysInteger.get(i + 1) + "");
		return dataObjectFirst;
	}

	/*
	 * 	@author : Nikhil
	 *  Method is created for PE-4843.
	 *	
	 *	@comment: SonarQube Changes for this method will be done by Nikhil
	 */
	public Map<String, Object> jobInversion() throws SQLException {
		logger.info("{}","Inside method jobInversion()");
		
		List<String[]> csvData = this.csvData;
		logger.info( "csvData :: {}", csvData);

		Map<String, Object> otherDataMap = this.otherDataMap;
		logger.info("otherDataMap :: {}", otherDataMap);
		
		JSONArray jsonArr = new JSONArray();
		JSONArray jsonArrLink = null;
		JSONObject jsonObj = null;
		
		Map<String, ArrayList<Double>> masterMap = new TreeMap<>();
		Map<String,Double> poscodeMedianMap = new HashMap<>();
		ArrayList<Double> tempSalArray = new ArrayList<>();
		
		int annualbaseColnum = -1;
		int monthbaseColnum = -1;
		int posCodeColnum = -1;
		int numMonthColnum = -1;
		int totalGuaranteedCashColNum = -1;	// PE-5320
		
		String comparisonOperator = "";
		double numMonths = ExpressionBuilderConstants.JOBINVERSION_DEFAULT_NUM_MONTHS;
		double tempSal = -1;
		boolean noSalaryFound = true;
		
		String tempCareer = "";
		String tempFamily="";
		String[] csvDataRow = null;
		String tempNxtCareer = "";
		String tempNxtFamily = ""; 
		boolean updatedFlag = false;
		boolean insertedFlag = false;
		boolean isError = false;
		
		ArrayList<Double> tempSalArr = null;
		Set<String> familySet = new TreeSet<>();
		
		double deviation = 0;
		boolean isInverse = false;
		
		DerbyConnection jdbcInstance = DerbyConnection.getInstance();
		Connection conn = null;
		ResultSet rs = null;
		String query = null;
		String[] args = null;
		List<String> poscodes = new ArrayList<>();

		String compensation = ExpressionBuilderConstants.SALARY_TYPE_REGULAR;	// Default Regular
		String ctryCode = null;

		try {
			if(null!=otherDataMap && otherDataMap.containsKey(ExpressionBuilderConstants.COUNTRYCODE) 
					&& null!=otherDataMap.get(ExpressionBuilderConstants.COUNTRYCODE)) {
				ctryCode = otherDataMap.get(ExpressionBuilderConstants.COUNTRYCODE).toString();
			}
			
			if(conn==null) {
				conn = MercerDBCP.getDataSource().getConnection();
				logger.info("{}",CONNECTION_TO_MERCER_DB_IS_OPENED);
			}
			
			query = " SELECT aggregatecheck FROM country_aggregatecheck_map WHERE countryCode = ? ";
			logger.info("Query executed :: {}", query);
			
			if(null!=ctryCode) {
				args = new String[]{ctryCode};
				rs = jdbcInstance.get(conn, query, args);
				if(rs.next()) {
					compensation = rs.getString("aggregatecheck");
				}
				logger.info("__COMPENSATION :: {}", compensation);			
			}
			
			if(compensation.equals(ExpressionBuilderConstants.SALARY_TYPE_TOTAL_GUARANTEED_CASH)) {
				// Find RowNums for corresponding Cols from 1st row.
				for(int i=0; i<csvData.get(0).length ; i++) {

					//Check Improve some performance
					switch (csvData.get(0)[i]) {
					case ExpressionBuilderConstants.COL_TOTAL_GUARANTEED_CASH:
						totalGuaranteedCashColNum = i;
						break;
					case ExpressionBuilderConstants.POS_CODE: 
						posCodeColnum = i;
						break;
					default : 
						break;
					}
				}

			} else {	
				
				// Find RowNums for corresponding Cols from 1st row.
				for(int i=0; i<csvData.get(0).length ; i++) {
		
					switch (csvData.get(0)[i]) {
					case ExpressionBuilderConstants.COL_ANNUAL_BASE: 
						annualbaseColnum = i;
						break;
					case ExpressionBuilderConstants.COL_MONTH_BASE: 
						monthbaseColnum = i;
						break;
					case ExpressionBuilderConstants.POS_CODE: 
						posCodeColnum = i;
						break;
					case ExpressionBuilderConstants.COL_NUM_MONTH:
						numMonthColnum = i;
						break;
					default :
						break;
					}
				}
			}
			
			if(-1!=posCodeColnum) {
			// Create "masterMap" as HashMap<PosCode,ArrayList<Salary>>
			for(int i=1; i<csvData.size(); i++) {
				noSalaryFound = true;
				tempSalArray = null;
				csvDataRow = csvData.get(i);
				
				// PE- Ignore record if no POSCODE/ANNUAL_SAL/BASE_SAL (Mail on 180213)
				if(null!=csvDataRow && csvDataRow[posCodeColnum]!=null && !"".equals(csvDataRow[posCodeColnum])
						&& csvDataRow[posCodeColnum].matches(ExpressionBuilderConstants.POS_CODE_REGEX)) {	
					try {
						// For __Compensation = TOTAL_GUARANTEED_CASH
						if(compensation.equals(ExpressionBuilderConstants.SALARY_TYPE_TOTAL_GUARANTEED_CASH)) {
							if( totalGuaranteedCashColNum!=-1 && (null!=csvDataRow[totalGuaranteedCashColNum] && !"".equals(csvDataRow[totalGuaranteedCashColNum])) ) {
								tempSal = Double.parseDouble(csvDataRow[totalGuaranteedCashColNum].toString());
									logger.info(SALARY_CALCULATED_FOR_ROW, i, tempSal);
									noSalaryFound = false;
							}
						
						// For __Compensation = REGULAR type, Find Salary from ANNUAL_BASE / MONTH_BASE,NUM_MONTH
						} else {
							if( annualbaseColnum!=-1 && (null!=csvDataRow[annualbaseColnum] && !"".equals(csvDataRow[annualbaseColnum])) ) {
								tempSal = Double.parseDouble(csvDataRow[annualbaseColnum].toString());
								logger.info(SALARY_CALCULATED_FOR_ROW, i, tempSal);
								noSalaryFound = false;
							
							}else if (monthbaseColnum!=-1 && (null!=csvDataRow[monthbaseColnum] && !"".equals(csvDataRow[monthbaseColnum]))) { 
				
								// Find Salary from MONTH_BASE and NUM_MONTH
								if(numMonthColnum!=-1 && (null!=csvDataRow[numMonthColnum] && !"".equals(csvDataRow[numMonthColnum])) ) {
									tempSal = Double.parseDouble(csvDataRow[monthbaseColnum].toString())
											* Double.parseDouble(csvDataRow[numMonthColnum].toString());
									logger.info(SALARY_CALCULATED_FOR_ROW, i, tempSal);
									noSalaryFound = false;
								
								// Find Salary from MONTH_BASE and "num_months" in MercerDB
								// PE-7110 : If not found in MercerDB, consider default : 12 months
								} else {
									if(conn==null) {
										conn = MercerDBCP.getDataSource().getConnection();
										logger.info(CONNECTION_TO_MERCER_DB_IS_OPENED);
									}
									
									if(null!=ctryCode) {
										args = new String[]{ctryCode};
										query = " SELECT def_num_months FROM ops_def_num_months WHERE ctx_ctry_code = ? ";
										logger.info("Query executed :: {}", query);
										
										rs = jdbcInstance.get(conn, query, args);
										
										if(rs.next()) {
											numMonths = rs.getDouble(1);
											logger.info("Query output :: {}", numMonths);
										}
									}
									tempSal = Double.parseDouble(csvDataRow[monthbaseColnum].toString()) * numMonths;
									logger.info(SALARY_CALCULATED_FOR_ROW, i, tempSal);
									noSalaryFound = false;
								}
							}
						}
						
						if(!noSalaryFound) {
							
							// Put salary in "masterMap" for the Poscode
							if(csvDataRow[posCodeColnum]!=null && masterMap.containsKey(csvDataRow[posCodeColnum])) {
								tempSalArray = masterMap.get(csvDataRow[posCodeColnum].toString());
								tempSalArray.add(tempSal);
							} else if(csvDataRow[posCodeColnum]!=null && !(csvDataRow[posCodeColnum].equals("null"))){	
								tempSalArray = new ArrayList<>();
								tempSalArray.add(tempSal);
								masterMap.put(csvDataRow[posCodeColnum], tempSalArray);
								
								// add unique family in the set.
								familySet.add(csvDataRow[posCodeColnum].split(ExpressionBuilderConstants.REGEX_FILENAME_SPLIT)[0]); 
							}
						}
					} catch(NumberFormatException e) {
						logger.error("Record ignored as string value is found instead of a number. StackTrace : {}", e.getMessage());
					} catch(Exception e) {
						logger.error("Record ignored. StackTrace : {}", e.getMessage());
					}
				}
			}
			logger.info("familySet :: {}", familySet);
			logger.info("masterMap :: {}", masterMap);
			
			// Create "poscodeMedianMap"
			for(Map.Entry<String, ArrayList<Double>> masterMapRec : masterMap.entrySet()) {
				tempSalArr = masterMapRec.getValue();
				double median;
				Collections.sort(tempSalArr);
				
				if (tempSalArr.size() % 2 == 0) {
				    median = ( (double)tempSalArr.get(tempSalArr.size()/2 - 1) + (double)tempSalArr.get(tempSalArr.size()/2) )/2;
				}else {
				    median = (double) tempSalArr.get(tempSalArr.size()/2);
				}
				poscodeMedianMap.put(masterMapRec.getKey(), median);
			}
			

			logger.info("poscodeMedianMap :: {}", poscodeMedianMap);
			
			if(conn==null) {
				conn = MercerDBCP.getDataSource().getConnection();
				logger.info("{}",CONNECTION_TO_MERCER_DB_IS_OPENED);
			}
			
			// Create keySet of PosCodes from "masterMap"
			poscodes.addAll(masterMap.keySet());
			
			logger.info("poscodes :: {}", poscodes);
			
			//Start PE-7474, Method call for sorting of POS code.
			List<String> sortedList = DataUtil.sortedList(poscodes,(TreeSet<String>) familySet);
			//End PE-7474.
			
			// Compare medians and create "jsonArr"
			// Iterate with a poscode
			for(int poscodeCtr=0; poscodeCtr<sortedList.size(); poscodeCtr++) {
				tempCareer = sortedList.get(poscodeCtr).split(ExpressionBuilderConstants.REGEX_FILENAME_SPLIT)[1];
				tempFamily = sortedList.get(poscodeCtr).split(ExpressionBuilderConstants.REGEX_FILENAME_SPLIT)[0];
				insertedFlag = false;

				// Iterate with a further poscodes
				for(int nxtPoscodeCtr=poscodeCtr+1; nxtPoscodeCtr<sortedList.size(); nxtPoscodeCtr++) {
					jsonObj = new JSONObject();
					jsonArrLink = new JSONArray();
					
					updatedFlag = false;
					isError = false;
					isInverse = false;
					
					tempNxtCareer = sortedList.get(nxtPoscodeCtr).split(ExpressionBuilderConstants.REGEX_FILENAME_SPLIT)[1];
					tempNxtFamily = sortedList.get(nxtPoscodeCtr).split(ExpressionBuilderConstants.REGEX_FILENAME_SPLIT)[0];
					
					// PE-7033 : Prepare JobInversion errors within same family
					if(tempFamily.equals(tempNxtFamily)) {	
						// Get comparison between this and further poscode from Mercerdb
						query = "select comparison from jobinversion where careerFrom=? and careerTo=?";
						args = new String[]{tempCareer, tempNxtCareer};
						rs = jdbcInstance.get(conn, query, args);
						
						if(rs.next()) {
							comparisonOperator = rs.getString(1);
							logger.info("Comparison done :: {}, {}, {}", tempNxtCareer, comparisonOperator,
									tempCareer);
							}else {
							args = new String[]{tempNxtCareer, tempCareer};
							rs = jdbcInstance.get(conn, query, args);
							if(rs.next()) {
								comparisonOperator = rs.getString(1);
								isInverse = true;
								logger.info("Comparison done :: {}, {}, {}", tempNxtCareer, comparisonOperator,
											tempCareer);
								}					
						}
						
						switch (comparisonOperator) {
						case ExpressionBuilderConstants.GREATER_THAN :
							if(!isInverse && 
										!(poscodeMedianMap.get(sortedList.get(poscodeCtr)) > poscodeMedianMap.get(sortedList.get(nxtPoscodeCtr))) ) {							
								isError = true;
							}else if(isInverse && 
										!(poscodeMedianMap.get(sortedList.get(nxtPoscodeCtr)) > poscodeMedianMap.get(sortedList.get(poscodeCtr)))) {
								isError = true;
							}
							break;
							
						case ExpressionBuilderConstants.LESS_THAN :
							if(!isInverse && 
									!(poscodeMedianMap.get(sortedList.get(poscodeCtr)) < poscodeMedianMap.get(sortedList.get(nxtPoscodeCtr)))) {
								isError = true;
							}else if(isInverse && 
									!(poscodeMedianMap.get(sortedList.get(nxtPoscodeCtr)) < poscodeMedianMap.get(sortedList.get(poscodeCtr))) ) {
								isError = true;
							}
							break;
							
						case ExpressionBuilderConstants.GREATER_THAN_OR_EQUALS :
							if(!isInverse) {
								deviation = poscodeMedianMap.get(sortedList.get(nxtPoscodeCtr)) * 0.1;
								if(!(poscodeMedianMap.get(sortedList.get(poscodeCtr)) >= 
										(poscodeMedianMap.get(sortedList.get(nxtPoscodeCtr)) - deviation)) ) {
									isError = true;
								}
							}else {
								deviation = poscodeMedianMap.get(sortedList.get(poscodeCtr)) * 0.1;
								if(!(poscodeMedianMap.get(sortedList.get(nxtPoscodeCtr)) >= 
										(poscodeMedianMap.get(sortedList.get(poscodeCtr)) - deviation)) ) {
									isError = true;
								}
							}
							break;
						default:logger.info("{}","In the default case");
							break;
						}	
						logger.info("Comparison done error :: {}", isError);
						
						if(isError) {
		
							// If current Poscode exists in "jsonArr" then update its LINKED
							for (int arrCtr = 0; arrCtr < jsonArr.length(); arrCtr++) {
								JSONObject tempObj = (JSONObject) jsonArr.get(arrCtr);
								if (sortedList.get(poscodeCtr)
										.equals(tempObj.get(ExpressionBuilderConstants.POS_CODE))) {
									JSONArray linkedArray = (JSONArray) tempObj.get(ExpressionBuilderConstants.LINKED);
									linkedArray.put(sortedList.get(nxtPoscodeCtr));
									updatedFlag = true;
								}
							}
							
							// Create new jsonObj for new posCode
							if(!updatedFlag) {
								jsonObj.put(ExpressionBuilderConstants.POS_CODE, sortedList.get(poscodeCtr));
								jsonObj.put(ExpressionBuilderConstants.MEDIAN, poscodeMedianMap.get(sortedList.get(poscodeCtr)) );
								jsonObj.put(ExpressionBuilderConstants.COUNT, masterMap.get(sortedList.get(poscodeCtr)).size());
								
								jsonArrLink.put(sortedList.get(nxtPoscodeCtr));
								jsonObj.put(ExpressionBuilderConstants.LINKED, jsonArrLink);
								jsonArr.put(jsonObj);
								
								insertedFlag = true;
							}
						}
					}
				}
				if (!insertedFlag) {
					jsonObj = new JSONObject();
					jsonObj.put(ExpressionBuilderConstants.POS_CODE, sortedList.get(poscodeCtr));
					jsonObj.put(ExpressionBuilderConstants.MEDIAN, poscodeMedianMap.get(sortedList.get(poscodeCtr)));
					jsonObj.put(ExpressionBuilderConstants.COUNT, masterMap.get(sortedList.get(poscodeCtr)).size());
					jsonObj.put(ExpressionBuilderConstants.LINKED, new JSONArray());
					jsonArr.put(jsonObj);
				}
				
			}
			} else {
				logger.error("{}","Mandatory columns not found.");				
			}
		}catch(SQLException e) {
			logger.error(" Exception occured .. {}",e.getMessage());
			throw new SQLException(e);
		}finally {
			if(null!=rs) {
				rs.close();
				logger.info("{}","ResultSet to MercerDB is closed...");
			}
			if(null!=conn) {
                conn.close();
                logger.info("{}","Connection to MercerDB is closed...");
            }
		}
		logger.info("jsonArr :: {}", jsonArr);
		logger.info("{}"," Exiting from  : >> jobInversion()");
		
		Map<String, Object> returnMap = new HashMap<>();
		returnMap.put(ExpressionBuilderConstants.CONST_INVERSION_JSON_ARRAY, jsonArr);
		returnMap.put(ExpressionBuilderConstants.CONST_COUNTRYCODE, ctryCode);
		returnMap.put(ExpressionBuilderConstants.CONST_COMPENSATION, compensation);
		return returnMap;
	}
}
