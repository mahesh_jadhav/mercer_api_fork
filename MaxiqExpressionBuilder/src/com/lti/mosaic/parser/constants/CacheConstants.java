package com.lti.mosaic.parser.constants;

/***
 * Contains constants required to store and fetch cache objects.
 * 
 * @author vivek K
 * 
 */
public class CacheConstants {

	private CacheConstants() {
	}
	
	public static final String MAXIQ_HOME = "MAXIQ_HOME";

	
	// MERCER DB CONFIG
	public static final String MERCER_DB_DRIVER = "MERCER_DB_DRIVER";
	public static final String MERCER_DB_ADDRESS = "MERCER_DB_ADDRESS";
	public static final String MERCER_DB_NAME = "MERCER_DB_NAME";			// "mercerdb"
	public static final String MERCER_DB_USERNAME = "MERCER_DB_USERNAME";
	public static final String MERCER_DB_PRIVATEKEY = "MERCER_DB_PASSWORD";
	
	// AD login config
	public static final String  AUTH_METHOD_AD="AD";
	public static final String  AD_SERVER_PATH="AD_SERVER_PATH";
	
}
