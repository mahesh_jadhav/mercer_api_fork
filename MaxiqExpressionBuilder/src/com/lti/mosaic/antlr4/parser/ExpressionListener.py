# Generated from Expression.g4 by ANTLR 4.7.1
from antlr4 import *

# This class defines a complete listener for a parse tree produced by ExpressionParser.
class ExpressionListener(ParseTreeListener):

    # Enter a parse tree produced by ExpressionParser#parse.
    def enterParse(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#parse.
    def exitParse(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#block.
    def enterBlock(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#block.
    def exitBlock(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#assignmentExpr.
    def enterAssignmentExpr(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#assignmentExpr.
    def exitAssignmentExpr(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#ifStatExpr.
    def enterIfStatExpr(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#ifStatExpr.
    def exitIfStatExpr(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#whileStatExpr.
    def enterWhileStatExpr(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#whileStatExpr.
    def exitWhileStatExpr(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#statementExpr.
    def enterStatementExpr(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#statementExpr.
    def exitStatementExpr(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#otherExpr.
    def enterOtherExpr(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#otherExpr.
    def exitOtherExpr(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#assignment.
    def enterAssignment(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#assignment.
    def exitAssignment(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#ifStat.
    def enterIfStat(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#ifStat.
    def exitIfStat(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#conditionBlock.
    def enterConditionBlock(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#conditionBlock.
    def exitConditionBlock(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#statBlock.
    def enterStatBlock(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#statBlock.
    def exitStatBlock(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#whileStat.
    def enterWhileStat(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#whileStat.
    def exitWhileStat(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#statement.
    def enterStatement(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#statement.
    def exitStatement(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#notExpr.
    def enterNotExpr(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#notExpr.
    def exitNotExpr(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#unaryMinusExpr.
    def enterUnaryMinusExpr(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#unaryMinusExpr.
    def exitUnaryMinusExpr(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#multiplicationExpr.
    def enterMultiplicationExpr(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#multiplicationExpr.
    def exitMultiplicationExpr(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#atomExpr.
    def enterAtomExpr(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#atomExpr.
    def exitAtomExpr(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#orExpr.
    def enterOrExpr(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#orExpr.
    def exitOrExpr(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#additiveExpr.
    def enterAdditiveExpr(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#additiveExpr.
    def exitAdditiveExpr(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#powExpr.
    def enterPowExpr(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#powExpr.
    def exitPowExpr(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#relationalExpr.
    def enterRelationalExpr(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#relationalExpr.
    def exitRelationalExpr(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#equalityExpr.
    def enterEqualityExpr(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#equalityExpr.
    def exitEqualityExpr(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#functionExpr.
    def enterFunctionExpr(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#functionExpr.
    def exitFunctionExpr(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#andExpr.
    def enterAndExpr(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#andExpr.
    def exitAndExpr(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#zeroParamterFunctions.
    def enterZeroParamterFunctions(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#zeroParamterFunctions.
    def exitZeroParamterFunctions(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#singleParamterFunctions.
    def enterSingleParamterFunctions(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#singleParamterFunctions.
    def exitSingleParamterFunctions(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#twoParamterFunctions.
    def enterTwoParamterFunctions(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#twoParamterFunctions.
    def exitTwoParamterFunctions(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#threeParamterFunctions.
    def enterThreeParamterFunctions(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#threeParamterFunctions.
    def exitThreeParamterFunctions(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#fourParamterFunctions.
    def enterFourParamterFunctions(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#fourParamterFunctions.
    def exitFourParamterFunctions(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#NParametersFunctions.
    def enterNParametersFunctions(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#NParametersFunctions.
    def exitNParametersFunctions(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#functionParam0.
    def enterFunctionParam0(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#functionParam0.
    def exitFunctionParam0(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#functionParam1.
    def enterFunctionParam1(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#functionParam1.
    def exitFunctionParam1(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#functionParam2.
    def enterFunctionParam2(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#functionParam2.
    def exitFunctionParam2(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#functionParam3.
    def enterFunctionParam3(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#functionParam3.
    def exitFunctionParam3(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#functionParam4.
    def enterFunctionParam4(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#functionParam4.
    def exitFunctionParam4(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#functionParamN.
    def enterFunctionParamN(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#functionParamN.
    def exitFunctionParamN(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#argumentsN.
    def enterArgumentsN(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#argumentsN.
    def exitArgumentsN(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#arguments1.
    def enterArguments1(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#arguments1.
    def exitArguments1(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#arguments2.
    def enterArguments2(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#arguments2.
    def exitArguments2(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#arguments3.
    def enterArguments3(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#arguments3.
    def exitArguments3(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#arguments4.
    def enterArguments4(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#arguments4.
    def exitArguments4(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#parExpr.
    def enterParExpr(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#parExpr.
    def exitParExpr(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#arrayAtom.
    def enterArrayAtom(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#arrayAtom.
    def exitArrayAtom(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#numberAtom.
    def enterNumberAtom(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#numberAtom.
    def exitNumberAtom(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#booleanAtom.
    def enterBooleanAtom(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#booleanAtom.
    def exitBooleanAtom(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#idAtom.
    def enterIdAtom(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#idAtom.
    def exitIdAtom(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#stringAtom.
    def enterStringAtom(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#stringAtom.
    def exitStringAtom(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#nilAtom.
    def enterNilAtom(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#nilAtom.
    def exitNilAtom(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#value_list.
    def enterValue_list(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#value_list.
    def exitValue_list(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#arrayValues.
    def enterArrayValues(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#arrayValues.
    def exitArrayValues(self, ctx):
        pass


    # Enter a parse tree produced by ExpressionParser#arrayElementTypes.
    def enterArrayElementTypes(self, ctx):
        pass

    # Exit a parse tree produced by ExpressionParser#arrayElementTypes.
    def exitArrayElementTypes(self, ctx):
        pass


